'use strict';


let _ = require('lodash');
let core = require('../../libs/core');
let nodeExcel = require('excel-export');//关联excel-export模块
let moment = require('moment');//关联moment模块
let baseRenderPath = 'server/bardian/'; // 基本路径

/**
 * 基础公共服务
 **/
let commonServer = function(Model) {
    return {
        //列表
        list : function(req, res) {
            //查数据总数
            let _condition = Model.getCondition(req);
            Model.count(_condition).exec().then(function(total) {
                let query = Model.find(_condition,Model.getFindFields()).populate(Model.getPopulate());
                //分页
                let pageInfo = core.createPage(req.query.page, total);
                //console.log(pageInfo);
                query.skip(pageInfo.start);
                query.limit(pageInfo.pageSize);
                query.sort(Model.getSort(req));
                query.exec(function(err, results) {
                    console.log(err, results);
                    console.log("==results="+ JSON.stringify(results));
                    let renderPath = baseRenderPath + Model.getModelFileName() + '/list';
                    res.render(renderPath,{
                        title: Model.getPageTitle(),
                        modelFileName: Model.getModelFileName(),
                        headerCols: Model.getHeaderCols(),
                        results: results,
                        pageInfo: pageInfo,
                        condition:_condition,
                        Menu: 'list'
                    });
                });
            });
        },

        //列表导出
        exportData : function(req, res) {
            // 查询条件
            let _condition = Model.getCondition(req);
            //查数据总数
            let query = Model.find(_condition);
            //console.log(pageInfo);
            query.skip(0);
            query.limit(99999);
            query.sort(Model.getSort(req));
            query.exec(function(err, results) {

                var conf = {};
                conf.cols = Model.getHeaderCols();

                // 以下为将数据封装成array数组。因为下面的方法里头只接受数组。
                var vac = new Array();
                for (var i = 0; i < results.length; i++) {
                    var temp = Model.getTempData(i,results[i]);
                    if (temp){
                        vac.push(temp);
                    }
                };
                console.log("===vac="+JSON.stringify(vac));
                conf.rows = vac;//conf.rows只接受数组
                var result = nodeExcel.execute(conf);
                res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                res.setHeader("Content-Disposition", "attachment; filename=exportReport.xlsx");
                res.end(result, 'binary');
            });
        },

        //添加
        add : function(req, res) {
            if (req.method === 'GET') {
                let obj = Model.getPickData(req);
                let renderPath = baseRenderPath + Model.getModelFileName() + '/add';
                res.render(renderPath,{
                    Menu: 'add',
                    result: obj,
                    modelFileName: Model.getModelFileName()
                });
            } else if (req.method === 'POST') {
                let obj = Model.getPickData(req);
                console.log("==obj="+ JSON.stringify(obj));
                let model = new Model(obj);
                model.save(function(err, result) {
                    console.log("err="+err);
                    if (req.xhr) {
                        return res.json({
                            status: !err
                        });
                    }

                    if (err) {
                        console.log(err);
                        return res.render('server/info', {
                            message: '创建失败,err:'+err
                        });
                    }

                    res.render('server/info', {
                        message: '创建成功'
                    });
                });
            }
        },

        edit : function(req, res) {
            if(req.method === 'GET') {
                let id = req.params.id;
                Model.findById(id).exec(function(err, result) {
                    if(err) {
                        console.log('加载内容失败');
                    }
                    // console.log("==result="+ JSON.stringify(result));
                    let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
                    if(!isAdmin) {
                        return res.render('server/info', {
                            message: '没有权限'
                        });
                    }
                    //console.log(tags)
                    let renderPath = baseRenderPath + Model.getModelFileName() + '/edit';
                    res.render(renderPath, {
                        result: result,
                        title: Model.getPageTitle(),
                        modelFileName: Model.getModelFileName(),
                        Menu: 'edit'
                    });
                });
            } else if(req.method === 'POST') {
                let id = req.params.id;
                let obj = Model.getPickData(req);
                console.log(obj);

                Model.findById(id).exec(function(err, result) {
                    //console.log(result);
                    let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

                    if(!isAdmin) {
                        return res.render('server/info', {
                            message: '没有权限'
                        });
                    }
                    _.assign(result, obj);
                    result.save(function(err, result2) {
                        if (req.xhr) {
                            return res.json({
                                status: !err
                            })
                        }
                        if(err || !result2) {
                            return res.render('server/info', {
                                message: '修改失败'
                            });
                        }
                        res.render('server/info', {
                            message: '更新成功'
                        });
                    });
                });
            }
        },

        //删除
        del : function(req, res) {
            let id = req.params.id;
            Model.findById(id).exec(function(err, result) {
                if(err || !result) {
                    return res.render('server/info', {
                        message: '内容不存在'
                    });
                }
                let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

                if(!isAdmin ) {
                    return res.render('server/info', {
                        message: '没有权限'
                    });
                }
                //
                result.remove(function(err) {
                    if (req.xhr) {
                        return res.json({
                            status: !err
                        });
                    }
                    if(err) {
                        return res.render('server/info', {
                            message: '删除失败'
                        });
                    }
                    res.render('server/info', {
                        message: '删除成功'
                    })
                });
            });
        }
    }
}

module.exports = commonServer;
