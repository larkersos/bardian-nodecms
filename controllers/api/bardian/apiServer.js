'use strict';

let apiServer = function(Model) {
    return {
        list: async function(req, res) {
            let _condition = Model.getCondition(req);
            let data = null
            let error
            try {
                data = await Model.find(_condition,Model.getFindFields()).populate(Model.getPopulate());
                // console.log("===============_condition="+ JSON.stringify(_condition));
            } catch (e) {
                // error = e.message
                console.error(e)
                error = '系统异常'
            }
            if (data == null)
                data = "这是一个NULL"

            res.json({
                success: !error,
                data: data,
                error: error
            });
        },

        one: async function(req, res) {
            let id = req.params.id;
            let data = null
            let error
            try {
                data = await
                Model.findOne({_id: id},Model.getFindFields()).populate(Model.getPopulate());
            } catch (e) {
                // error = e.message
                console.error(e)
                error = '系统异常,e:'+e.message
            }
            if (data == null)
                data = "这是一个NULL"

            res.json({
                success: !error,
                data: data,
                error: error
            });
        },
        update : async function(req, res) {
            let id = req.params.id
            //let obj = req.body
            let obj = Model.getCondition(req);

            //let user = req.user
            //if (user) {
            //    obj.author = mongoose.Types.ObjectId(user._id)
            //}
            // TODO： 校验输入
            let data = null
            let error
            try {
                //let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
                //let item = await Model.findById(id)
                //let isAuthor = !!(item.author && ((item.author + '') === (req.user._id + '')))
                //if(!isAdmin && !isAuthor) {
                //    error = '没有权限'
                //} else {
                //    data = await Model.findByIdAndUpdate(id, obj, {new: true})
                //}
                data = await Model.findByIdAndUpdate(id, obj, {new: true}).populate(Model.getPopulate());
            } catch (e) {
                // error = e.message
                error = '更新失败,e:'+e.message
            }
            res.json({
                success: !error,
                data: data,
                error: error
            })
        },
        create : async function(req, res) {
            let obj = Model.getCondition(req);
            let user = req.user
            if (user) {
                obj.created_user = user.name
            }
            // TODO： 校验输入
            let data = null
            let error
            try {
                let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
                //let isAuthor = !!(item.author && ((item.author + '') === (req.user._id + '')))
                //if(!isAdmin && !isAuthor) {
                //    error = '没有权限'
                //} else {
                //    data = await Model.create(obj)
                //}
                data = await Model.create(obj)
            } catch (e) {
                // error = e.message
                error = '创建失败，err:'+e.message
            }
            res.json({
                success: !error,
                data: data,
                error: error
            })
        }
    }
}

module.exports = apiServer;