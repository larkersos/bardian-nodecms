'use strict';

let mongoose = require('mongoose')
let modelService = require('../../../services/bardian/vote')
let MongoModel = mongoose.model('Vote')

// 查询列表
exports.list = async function(req, res) {
    let _condition = MongoModel.getCondition(req);
    let data = null
    let error
    try {
        data = await modelService.find(_condition,['name','city','id','created_time']);
    } catch (e) {
        // error = e.message
        console.error(e)
        error = '系统异常'
    }
    if (data == null)
        data = "这是一个NULL"
    
    res.json({
        success: !error,
        data: data,
        error: error
    });
}

// ID查询单个
exports.one = async function(req, res) {
    let id = req.params.id;
    let data = null
    let error
    try {
        data = await modelService.findOne({_id:id},['name','city','id']);
    } catch (e) {
        // error = e.message
        console.error(e)
        error = '系统异常'
    }
    if (data == null)
        data = "这是一个NULL"

    res.json({
        success: !error,
        data: data,
        error: error
    });
}

// 创建
exports.create = async function(req, res) {
    // let obj = req.body
    let obj = MongoModel.getPickData(req);
    // TODO： 校验输入
    // 后台创建用户
    let user = req.user
    if (user) {
        obj.author = mongoose.Types.ObjectId(user._id)
    }
    let data = null
    let error
    try {
        data = await modelService.create(obj)
    } catch (e) {
        //error = e.message
        error = '创建失败,e:'+e.message
    }
    res.json({
        success: !error,
        data: data,
        error: error
    })
}

// 更新
exports.update = async function(req, res) {
    let id = req.params.id
    //let obj = req.body
    let obj = MongoModel.getCondition(req);

    //let user = req.user
    //if (user) {
    //    obj.author = mongoose.Types.ObjectId(user._id)
    //}
    // TODO： 校验输入
    let data = null
    let error
    try {
        //let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
        //let item = await Model.findById(id)
        //let isAuthor = !!(item.author && ((item.author + '') === (req.user._id + '')))
        //if(!isAdmin && !isAuthor) {
        //    error = '没有权限'
        //} else {
        //    data = await modelService.findByIdAndUpdate(id, obj, {new: true})
        //}
        data = await modelService.findByIdAndUpdate(id, obj, {new: true})
    } catch (e) {
        // error = e.message
        error = '更新失败'
    }
    res.json({
        success: !error,
        data: data,
        error: error
    })
}
