/**
 * 等级注册服务
 **/
'use strict';

let mongoose = require('mongoose');
let _ = require('lodash');
let Model = mongoose.model('Register');


let baseServices = require('../base')(Model);

let services = {
};

module.exports = _.assign({}, baseServices, services);

