'use strict';


let express = require('express')
let router = express.Router()
let core = require('../../../libs/core')
let action = require('../../../middlewares/action')
let controllerServer = require('../../../controllers/server/bardian/register')

//权限判断
router.use(function(req, res, next) {
    console.log('登记注册页: ' + Date.now());
    res.locals.Path = 'register';
    if(!req.session.user) {
        let path = core.translateAdminDir('/user/login');
        return res.redirect(path);
    }
    next();
});

// 列表
router.route('/').get(action.checkAction('REGISTER_INDEX'), controllerServer.list);
router.route('/list').get(action.checkAction('REGISTER_INDEX'), controllerServer.list);
router.route('/list').post(action.checkAction('REGISTER_INDEX'), controllerServer.list);
router.route('/exportData').post(action.checkAction('REGISTER_EXPORT'), controllerServer.exportData);
//添加
router.route('/add').all(action.checkAction('REGISTER_CREATE'), controllerServer.add);
//更新
router.route('/:id/edit').all(action.checkAction('REGISTER_UPDATE'), controllerServer.edit);
//删除
router.route('/:id/del').all(action.checkAction('REGISTER_DELETE'), controllerServer.del);

module.exports = function(app) {
    let path = core.translateAdminDir('/bardian/register');
    app.use(path, router);
};
