'use strict';


let express = require('express')
let router = express.Router()
let core = require('../../../libs/core')
let action = require('../../../middlewares/action')
let controllerServer = require('../../../controllers/server/bardian/vote')

//权限判断
router.use(function(req, res, next) {
    console.log('投票页: ' + Date.now());
    res.locals.Path = 'vote';
    if(!req.session.user) {
        let path = core.translateAdminDir('/user/login');
        return res.redirect(path);
    }
    next();
});

// 列表
router.route('/').get(action.checkAction('VOTE_INDEX'), controllerServer.list);
router.route('/list').get(action.checkAction('VOTE_INDEX'), controllerServer.list);
router.route('/list').post(action.checkAction('VOTE_INDEX'), controllerServer.list);
router.route('/exportData').post(action.checkAction('VOTE_EXPORT'), controllerServer.exportData);
//添加
router.route('/add').all(action.checkAction('VOTE_CREATE'), controllerServer.add);
//更新
router.route('/:id/edit').all(action.checkAction('VOTE_UPDATE'), controllerServer.edit);
//删除
router.route('/:id/del').all(action.checkAction('VOTE_DELETE'), controllerServer.del);

module.exports = function(app) {
    let path = core.translateAdminDir('/bardian/vote');
    app.use(path, router);
};
