'use strict';

let express = require('express')
let router = express.Router()
let jwtMiddleWare = require('../../../middlewares/jwt')
let action = require('../../../middlewares/action')

let apiService = require('../../../controllers/api/bardian/register')
//
router.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Request-With,content-type,Authorization')
    next();
});


router.route('/')
    .get(apiService.list)
    .post( apiService.create)

router.route('/:id')
    .get(apiService.one)
    //.put(jwtMiddleWare.verify, action.checkAction('USER_UPDATE'), apiService.update)
    .put(apiService.update)

module.exports = function(app) {
    router.use(function(req, res) {
        res.json({
            success: false,
            error: '无效请求'
        })
    });
    app.use('/api/bardian/register', router);
};
