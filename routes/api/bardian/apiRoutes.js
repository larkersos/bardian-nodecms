'use strict';

let apiRoutesService = function(apiService) {
    return {
        getRouter: function (router) {
            router.use(function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
                res.setHeader('Access-Control-Allow-Headers', 'X-Request-With,content-type,Authorization');
                next();
            });

            router.route('/')
                .get(apiService.list);

            router.route('/')
                .post(apiService.create);

            router.route('/:id')
                .get(apiService.one)
                //.put(jwtMiddleWare.verify, action.checkAction('USER_UPDATE'), apiService.update)
                .put(apiService.update);

            router.use(function (req, res) {
                res.json({
                    success: false,
                    error: '无效请求'
                })
            });

            return router;
        }
    }
}

module.exports = apiRoutesService;
