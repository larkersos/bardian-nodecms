'use strict';

/**
 * 模块依赖
 */
let _ = require('lodash');
let mongoose = require('mongoose')
let Schema = mongoose.Schema
let moment = require('moment');//关联moment模块
let modelStatics = require('../common-model-statics');//关联公共静态方法模块

/**
 * 投票模型
 */
let ModelSchema = new Schema({
    school: {//  所属学校
        type: Schema.ObjectId,
        ref: 'School'
    },
    year: {// 入学年份
        type: String,
        required: true
    },
    serial_no: {// 班级序号
        type: String,
        required: true
    },
    code: {// 班级编码
        type: String,
        required: true
    },
    name: {//名称
        type: String,
        required: true
    },
    master: {// 班主任
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

// 自定义方法 - 实例
ModelSchema.methods = {

};

// 自定义静态方法
let _staticsMethods = {
    /**
     * getModelName - get the model name
     * @return {String}
     * @api public
     */
    // 获得模块名称
    getModelFileName: function() {
        return "class";
    },
    getPopulate: function() {
        return "school";
    },
    // 获得模块页面名称
    getPageTitle: function() {
        return "班级管理";
    },
    // 获得数据排序
    getListSort: function() {
        return {term: 1};
    },
    // 获取导出表头
    getHeaderCols: function () {
        let cols=[
            {caption:'序号',field:'id', type:'number', width: 10},
            {caption:'学校',field:'school',  type:'string', width: 300},
            {caption:'年份',field:'year',  type:'number', width: 20},
            {caption:'学期',field:'term',  type:'number', width: 10},
            {caption:'开始时间',field:'begin_date',  type:'string', width: 200},
            {caption:'截止时间',field:'end_date',  type:'string', width: 200}

            ,{caption:'名称',field:'name',  type:'string', width: 300}
        ];
        return cols;
    },

    // 获取数据
    getTempData : function(i,result) {
        var temp = new Array();
        temp[0]= (i+1);
        temp[1]= result.school ? result.school.name : '';
        temp[2]= result.year;
        temp[3]= result.term;
        temp[4] = moment(result.begin_date).format("YYYY-MM-DD"); //格式串moment + "";
        temp[5] = moment(result.end_date).format("YYYY-MM-DD"); //格式串moment + "";
        temp[6] = result.name;
        return temp;
    },

    // 获取数据
    getPickData:function(req) {
        let _objData = {};
        let fields = ['school', 'year','code', 'serial_no', 'begin_date', 'end_date', 'name'];
        if (req.method === 'GET') {
            _objData = _.pick(req.query,fields);
        }else  {
            _objData = _.pick(req.body, fields);
        }
        return _objData;
    }
};
// 静态方法
ModelSchema.statics = _.assign({}, modelStatics, _staticsMethods);

mongoose.model('Vote', ModelSchema);