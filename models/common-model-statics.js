'use strict';

/**
 * 模块依赖
 */
let _ = require('lodash');

/**
 * 模块公共方法
 */
module.exports = {
    /**
     * getModelName - get the model name
     * @return {String}
     * @api public
     */
    // 获得模块名称
    getModelFileName: function() {
        return "";
    },
    getPopulate: function() {
        return "";
    },
    // 获得模块名称
    getPageTitle: function() {
        return "";
    },
    // 获得数据排序
    getListSort: function() {
        // 默认时间降序
        return {created_time: -1};
    },
    // 获取导出表头
    getHeaderCols: function () {
        let cols=[
            {caption:'序号', type:'string', width: 10},
            {caption:'caption1', type:'string', width: 10},
            {caption:'caption2', type:'string', width: 300},
            {caption:'created_time', type:'string', width: 300}
        ];
        return cols;
    },

    // 获取基本参数
    getSort : function(req) {
        let _sort = {};
        console.log("===============_sort="+ JSON.stringify(_sort));
        return _sort;
    },

    // 获取数据
    getTempData : function(i,result) {
        var temp = new Array();
        temp[0]= (i+1);
        temp[1]= result.term ? result.term : "";
        temp[2]= moment(result.created_time).format("YYYY-MM-DD"); //格式串moment + "";

        return temp;
    },

    // 获取可见字段
    getPickFields:function()  {
        let fields = ['term', 'start', 'end'];
        // console.log("===============_condition="+ JSON.stringify(_condition));
        return fields;
    },

    // 获取数据，包含空字符
    getPickData:function(req) {
        // let _objData = _.pick(req.body, 'term', 'start', 'end');
        let _objData = {};
        // 获取可见字段
        let fields = this.getPickFields();
        if (req.method === 'GET') {
            _objData = _.pick(req.query,fields);
        }else  {
            _objData = _.pick(req.body, fields);
        }
        return _objData;
    },

    // 获取基本参数,排除空字符
    getCondition:function(req)  {
        let _condition = _.assign({},this.getPickData(req));
        for (var item in _condition) {
            // console.log('item=='+item + ",_condition[item]="+_condition[item]);
            // 排除空字符
            if(_condition[item]==''){
                delete _condition[item];
            }
        }
        // console.log("===============_condition="+ JSON.stringify(_condition));
        return _condition;
    },

    // 获取排除字段
    baseOmitFields:function()  {
        let fields = ['__v'];
        // console.log("===============_condition="+ JSON.stringify(_condition));
        return fields;
    },

    // 获取查询结果字段
    getFindFields:function()  {
        // 字段结果
        let fields = {};
        // 排除字段
        let omitFields = this.baseOmitFields();
        // console.log("===============omitFields="+ JSON.stringify(omitFields));
        for (var index in omitFields) {
            // 排除字段
            fields[omitFields[index]] = 0;
        }
        // 自定义排除字段

        // console.log("===============fields="+ JSON.stringify(fields));
        return fields;
    }

};
