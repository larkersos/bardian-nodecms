module.exports = [
    {
        name: '登记注册管理',
        actions: [
            {
                name: '访问',
                value: 'REGISTER_INDEX',
                description: '角色访问'
            },{
                name: '创建',
                value: 'REGISTER_CREATE',
                description: '创建'
            },{
                name: '查看',
                value: 'REGISTER_DETAIL',
                description: '查看'
            },{
                name: '编辑',
                value: 'REGISTER_UPDATE',
                description: '编辑'
            },{
                name: '删除',
                value: 'REGISTER_DELETE',
                description: '删除'
            }
        ]
    },{
        name: '投票管理',
        actions: [
            {
                name: '访问',
                value: 'VOTE_INDEX',
                description: '访问'
            },{
                name: '创建',
                value: 'VOTE_CREATE',
                description: '创建'
            },{
                name: '查看',
                value: 'VOTE_DETAIL',
                description: '查看'
            },{
                name: '编辑',
                value: 'VOTE_UPDATE',
                description: '编辑'
            },{
                name: '删除',
                value: 'VOTE_DELETE',
                description: '删除'
            }
        ]
    },{
        name: '角色管理',
        actions: [
            {
                name: '角色访问',
                value: 'ROLE_INDEX',
                description: '访问'
            },{
                name: '创建角色',
                value: 'ROLE_CREATE',
                description: '创建角色'
            },{
                name: '查看角色信息',
                value: 'ROLE_DETAIL',
                description: '查看角色信息'
            },{
                name: '编辑角色',
                value: 'ROLE_UPDATE',
                description: '编辑角色'
            },{
                name: '删除角色',
                value: 'ROLE_DELETE',
                description: '删除角色'
            }
        ]
    },{
        name: '用户管理',
        actions: [
            {
                name: '用户访问',
                value: 'USER_INDEX',
                description: '用户访问'
            },{
                name: '创建用户',
                value: 'USER_CREATE',
                description: '创建用户'
            },{
                name: '查看用户信息',
                value: 'USER_DETAIL',
                description: '查看用户信息'
            },{
                name: '编辑用户',
                value: 'USER_UPDATE',
                description: '编辑用户'
            },{
                name: '删除用户',
                value: 'USER_DELETE',
                description: '删除用户'
            }
        ]
    },{
        name: '日志管理',
        actions: [
            {
                name: '日志访问',
                value: 'LOG_INDEX',
                description: '日志访问'
            },{
                name: '查看日志信息',
                value: 'LOG_DETAIL',
                description: '查看日志信息'
            },{
                name: '删除日志',
                value: 'LOG_DELETE',
                description: '删除日志'
            }
        ]
    },{
        name: '文件管理',
        actions: [
            {
                name: '文件访问',
                value: 'FILE_INDEX',
                description: '文件访问'
            },{
                name: '上传文件',
                value: 'FILE_CREATE',
                description: '上传文件'
            },{
                name: '查看文件信息',
                value: 'FILE_DETAIL',
                description: '查看文件信息'
            },{
                name: '编辑文件',
                value: 'FILE_UPDATE',
                description: '编辑文件'
            },{
                name: '删除文件',
                value: 'FILE_DELETE',
                description: '删除文件'
            }
        ]
    }
];